# COVID workshop material

If you need a runtime environment you can create a free account in IBM Cloud:

https://cloud.ibm.com/

Set up your environment this way:

https://github.com/IBM/skillsnetwork/wiki/Watson-Studio-Setup

## Introduction

In the sections below you find the *raw* links to use when importing a notebook from URL in the IBM Cloud environment. From within your project, click **Add to project**, **Notebook**, choose the tab **from URL**. Provide a name and paste one of the URLs below. Make sure you select a **Python 3.6** runtime, the `pyearth` library isn't compatible with 3.7 yet. You'll get deprecation warnings which you can ignore for now and click **Create**.

A workaround for Python 3.7 is to use the development branch of `pyearth`. Install this inside a notebook with:

`pip install git+https://github.com/scikit-learn-contrib/py-earth@v0.2dev`

*Best practice is to always run the full notebook first.*

From the text menu, click **Cell** and then **Run all**. Any installs will only be run once, that can take a while the first time. After running all cells you can look through the output and run individual cells with changed content by **Cell**, **Run cells** in the menu, click **Run** in the toolbar or press **Shift-Enter** when inside the cell.

If you change the code in a cell after running them all, for example in `zzcorwav.ipynb` changing the country code from `NL` to `BE`, you don't have to run all the cells above the current one. Just choose **Cell**, **Run all below** and check the results. If you are exploring data in `EUCDC.ipynb` you can just execute a cell to see the output after you have run them all.

I use two letter ISO codes for countries all over the place, here's a list:

https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2

Talking ISO, there's a problem with Greece, officially `GR` but EU CDC uses `EL`, so use the latter if you want to see their data. Not to mention problems with Namibia, country code `NA`.

## Jupyter notebooks to explore

Several notebooks are provided, you probably can't explore them all in one hour, but you can always come back for more!

If you want to dive straight in, you can start with `zzcorggd.ipynb` to generate an projections file for `zzcoradprd.ipynb` to plot a map. Just run the two notebooks after each other and see what you get!

### Getting to know the data

https://gitlab.com/dzwietering/corona/-/raw/master/pydata/EUCDC.ipynb

This notebook shows many examples of using Pandas dataframes to organize your data and interact with it. Have a look at the visualizations, change the code to show different countries and explore the EU CDC data.

Skip this notebook if you already know Pandas or if you just want the exciting stuff!

Update: I've included a version running on the OWID data, using 3 character ISO codes:

https://gitlab.com/dzwietering/corona/-/raw/master/pydata/OWID.ipynb

### Curve fitting using the Gumbel distribution

https://gitlab.com/dzwietering/corona/-/raw/master/pydata/Gumbelpivot.ipynb

After quite some research and discussion, and trying logistic and gamma distributions, I found that an outbreak usually follows a Gumbel distribution. To fit that to the data, we need some high school mathematics, linear regression and optimization as shown in the notebook. For now we only look at data for the first wave.

More information on the Gumbel distribution:

https://en.wikipedia.org/wiki/Gumbel_distribution

### Relating outbreak severity to country measures

https://gitlab.com/dzwietering/corona/-/raw/master/pydata/Gumbelmulti.ipynb  
https://gitlab.com/dzwietering/corona/-/raw/master/pydata/Measures.ipynb

For this analysis I use two notebooks that can be part of a pipeline, because the first one generates the dataset for the second. First we run the Gumbel curve fitting for all available countries, to derive a severity measure defined as the maximum number of concurrently infected people. Second, we use this measure and data from ACAPS to find relations between government measures and our severity measure. As we build an XGBoost model, we need an explainer to understand the model, which is where SHAP comes in.

Have a look at SHAP to understand the explainer output:

https://github.com/slundberg/shap

__*An important note here*__, don't jump to conclusions such as 'lockdowns have little effect'. You really need to dive into the details and create more sophisticated models to go public with that kind of statements. It could very well be that other measures are highly correlated and that a lockdown doesn't have much *additional* effect. Another pitfall is that missing values for a measure may be positive. Don't conclude that they aren't needed, it may be that those measures were already in effect so didn't need to be taken, such as improvements in health service. A country with adequate health service will not need an improvement measure and may also have a less severe outbreak.

### Clustering case data in multiple waves

https://gitlab.com/dzwietering/corona/-/raw/master/pydata/zzcorwav.ipynb

This is where you find my invention, don't try to understand it all at once but get a feeling of what it does by trying different countries and perhaps even tweak some selections or parameters. Can you spot the difference between The Netherlands and Belgium? The overal graphs look very similar, but things look different once you see the individual outbreaks.

### Generating local projections

https://gitlab.com/dzwietering/corona/-/raw/master/pydata/zzcorggd.ipynb

Using individual case data from RIVM you can run the analysis per GGD region. See how this data is not aggregated per day but contains one line per case, so we do the aggregation in the pivot.
Just start by running all cells (as always) and check the output in text and graphs. Note that, depending on how your environment buffers output data, it can take a couple of minutes to run before you see any output. This notebook produces an output file after a `melt` of the pivoted dataframe, so we get a result file similar to the original input, in this case containing projected data.

### Mapping projections

https://gitlab.com/dzwietering/corona/-/raw/master/pydata/zzcoradprd.ipynb

If you ran `zzcorggd.ipynb` in the section above you have generated a file with locally projected data, so run that notebook first. We read the generated file here and use Folium to put it on a map. Lots of data manipulation in this notebook to generate the correct input for the `HeatMapWithTime` plugin that we use to generate an animated map. Have a look at the parameters for the map and see how `radius` and `use_local_extrema` influence the display. Just change the code and run the cell to check the output.

Folium documentation, specifically for `HeatMapWithTime`:

https://python-visualization.github.io/folium/plugins.html

### Advanced visualization

https://gitlab.com/dzwietering/corona/-/raw/master/pydata/zzcorageprd.ipynb

Trying to cram as much data as possible into one visualization is rarely a good idea, but inspired by some examples and the [annotated heatmap](https://matplotlib.org/3.1.1/gallery/images_contours_and_fields/image_annotated_heatmap.html#sphx-glr-gallery-images-contours-and-fields-image-annotated-heatmap-py "matplotlib examples") from `matplotlib`, I created this to both see the overall clusters of cases over time and the detailed data in one chart. Have a good look at what it shows and try to tweak it to your taste.

### Conspiracy, anyone?

https://gitlab.com/dzwietering/corona/-/raw/master/pydata/zzbenford.ipynb

There's a little known rule called Benford's law that describes just about any naturally occurring phenomenon. In other words, if a dataset doesn't follow the rule, it's probably manipulated. In this small notebook we test the EU CDC data against Benford and find that any conspiracy would have to be very sophisticated and worldwide coordinated, because it applies to this dataset at both the global and continent level. Have a look for yourself, and perhaps find out that the country level data may seem manipulated, but don't worry, it's not supposed to follow the rule.

More about Benford's law:

https://en.wikipedia.org/wiki/Benford%27s_law

## Things to explore

I hope this material inspires you to explore further, please find some suggestions below.

### Using other input data

I started building on EU CDC data and just coded fast forward to get at the first result. Afterwards, I used local data from The Netherlands, Belgium, Spain, USA and others and cleaned up a bit to make that easier. Have a look if you can adapt `zzcorwav.ipynb` to your own data. If that's still a bit too complex, have a look at the my current notebook, containing lots of work on abstraction and parametrization:

https://gitlab.com/dzwietering/corona/-/blob/master/experiment/zzcorwav.ipynb

Again, if you want to run it in IBM Cloud, use the *raw* link. You'll see that I've switched to data from Our World in Data, as EU CDC no longer provides daily updates. There's also a version in the `params` folder where all configuration has been separated out, as a first move to having a general notebook with external parameters.

### Creating a website or application

For now, the notebooks you'll find here either provide direct feedback, produce an output file for further processing, or create HTML output such as a map that you could host on a web server. Perhaps you can pick this up and create a proper web site with interaction. Of course I've been working on that too, but I'm a terrible front end developer so for now I just cobble together some graphs and a map such as this one:

https://dzwietering.gitlab.io/coralert/zzcoralert.html

Have a look at the repository:

https://gitlab.com/dzwietering/coralert

This is just a version of a notebook using the Dutch case file to produce graphs in PNG format and an HTML map from `HeatMapWithTime`, with a script to embed that in some CSS. It's then pushed to git where a simple pipeline picks it up to generate a static HTML site. I hope you can do better!

### Expanding the Measures model

For now the model relating measures to outbreak severity is limited to the first wave, roughly using the data for the first months of the outbreak. It also looks at just the introduction of measures, while an early relaxation may also influence the severity. Perhaps you can take that into account and look at subsequent or even overlapping outbreaks.

### Combining more insights

My work on overlapping outbreaks is now integrated into one of the dashboards of the Emergent Alliance:

https://emergentalliance.org/products-and-services/cookie-cutter/

Have a look and see if you can combine even more data to understand what's going on.
